#!/bin/bash

if [ ! -d "logs" ]; then
mkdir logs
fi

mkdir -p logs/{imagenet,synthetic}
mkdir -p logs/imagenet/{fp16,fp32}
mkdir -p logs/synthetic/{fp16,fp32}

for o in resnet50 resnet152 alexnet googlenet inception3 inception4
do
gpu_num=`echo $(nvidia-smi --format=csv --query-gpu=count --format=csv,noheader | uniq -d)`
echo "running model: $o"

	for i in {1..$gpu_num}
	do
		echo "Running bench for GPU Numbers: $i"
		for j in 64 128 256 512 1024 2048
		do
		echo "Running Batch: $j"

# running synthetic fp16
python tf_cnn_benchmarks.py --num_gpus=$i --batch_size=$j --model=$o --variable_update=parameter_server --use_fp16=True > logs/synthetic/fp16/tf_synthetic_${i}GPU_BATCH${j}_MODEL${o}_fp16.txt

# running synthetic fp32
python tf_cnn_benchmarks.py --num_gpus=$i --batch_size=$j --model=$o --variable_update=parameter_server  > logs/synthetic/fp32/tf_synthetic_${i}GPU_BATCH${j}_MODEL${o}_fp32.txt

# running imagenet fp16
python tf_cnn_benchmarks.py --num_gpus=$i --batch_size=$j --model=$o --variable_update=parameter_server --nodistortions --gradient_repacking=8 --num_batches=100 --weight_decay=1e-4 --data_dir=/data/training/ --use_fp16=True --data_name=imagenet > logs/imagenet/fp16/tf_imagenet_${i}GPU_BATCH${j}_MODEL${o}_fp16.txt

# running imagenet fp32
python tf_cnn_benchmarks.py --num_gpus=$i --batch_size=$j --model=$o --variable_update=parameter_server --nodistortions --gradient_repacking=8 --num_batches=100 --weight_decay=1e-4 --data_dir=/data/training/ --data_name=imagenet > logs/imagenet/fp32/tf_imagenet_${i}GPU_BATCH${j}_MODEL${o}_fp32.txt

		done
	done

done