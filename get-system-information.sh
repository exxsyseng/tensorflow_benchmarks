#!/bin/bash

################################
# ========= Variables ==========
################################
. /etc/os-release

HOSTNAME=$(hostname)
FILE=${HOSTNAME}_${SN}_$(date +"%Y%m%d").txt

rm -rf /tmp/$FILE
>/tmp/$FILE
#chmod 777 $FILE


echo "#=======================================================================================#" >> /tmp/$FILE
echo "System Information" >> /tmp/$FILE
echo "#=======================================================================================#" >> /tmp/$FILE
echo " " >> /tmp/$FILE
echo "##### SYSTEM INFORMATION #####" >> /tmp/$FILE
echo "$(dmidecode -t system | egrep -A9 "System Information")" >> /tmp/$FILE
echo " " >> /tmp/$FILE
echo "##### MAKE #####" >> /tmp/$FILE
echo "$(dmidecode -s system-manufacturer)" >> /tmp/$FILE
echo " " >> /tmp/$FILE
echo "##### MODEL #####" >> /tmp/$FILE
echo "$(dmidecode -s system-product-name)" >> /tmp/$FILE
echo " " >> /tmp/$FILE
echo "##### SERIAL #####" >> /tmp/$FILE
echo "$(dmidecode -s system-serial-number)" >> /tmp/$FILE
echo " " >> /tmp/$FILE
echo "##### OS #####" >> /tmp/$FILE
echo "$ID $VERSION_$ID" >> /tmp/$FILE
echo " " >> /tmp/$FILE
echo "##### CPU #####" >> /tmp/$FILE
echo "$(dmidecode -s  processor-version)" >> /tmp/$FILE
echo "$(grep -E "cpu cores|siblings|physical id" /proc/cpuinfo | xargs -n 11 echo |sort |uniq)" >> /tmp/$FILE
echo " " >> /tmp/$FILE
echo "##### DISK #####" >> /tmp/$FILE
echo "$(lsblk -o NAME,SIZE,TYPE,MOUNTPOINT)" >> /tmp/$FILE
echo " " >> /tmp/$FILE
echo "##### MEMORY #####" >> /tmp/$FILE
echo "$(dmidecode -t memory | grep -i size | grep -v No)" >> /tmp/$FILE
echo " " >> /tmp/$FILE
echo "##### GPU INFORMATION #####" >> /tmp/$FILE
echo " " >> /tmp/$FILE
echo " --- Number of GPUs ---" >> /tmp/$FILE
echo " " >> /tmp/$FILE
echo "$(nvidia-smi --format=csv --query-gpu=count --format=csv,noheader | uniq -d)" >> /tmp/$FILE
echo " " >> /tmp/$FILE
echo " --- NAME, DRIVER, MEMORY, UTILIZATION, POWER, FAN, TEMP ---" >> /tmp/$FILE
echo "$(nvidia-smi --format=csv --query-gpu=name,driver_version,memory.total,utilization.gpu,power.draw,fan.speed,temperature.gpu)" >> /tmp/$FILE
echo " " >> /tmp/$FILE
echo " --- GPU Topology ---" >> /tmp/$FILE
echo " " >> /tmp/$FILE
echo "$(nvidia-smi topo -m)" >> /tmp/$FILE
echo " " >> /tmp/$FILE
echo "##### DOCKER IMAGES #####" >> /tmp/$FILE
echo " " >> /tmp/$FILE
echo "$(docker ps -a)" >> /tmp/$FILE
echo " " >> /tmp/$FILE




cd /tmp
exec bash
