TensorFlow benchmarks Tools
===

Automation tools for Tensorflow Benchmarking

## Overview
This repository contains TensorFlow benchmark scripts for running benchmark jobs and processing data outputs. 

## Releases

* Owner: Engineering
* Latest Revision [TF Benchmark v1.0](https://exxsyseng@bitbucket.org/exxsyseng/tensorflow_benchmarks.git)

### Contribution guidelines ###

* To modify, please issue a pull rerquest against the master branch from a local fork
```sh
# Issue a pull request 
git pull
```

* All scripts should be updated with Revision Date and Last Update by. Where revision date specifies the last date the script was modified and Update by contains the person that last modified the script.
```sh
# Revision v1
# Revision Date: 02/26/2020
# Last Updated by: Alexander Hill
```

### Usage ###

```sh
paralelle-20200122.tar.bz2				# Paralelle GNU software for data processing
run-all-benchmarks.sh					# Script for running all TF benchmarks and Parameters
process_data.sh							# Script for processing result files
copy_process_data.sh					# Script to copy the process_data.sh script to data results directories
```

### Workflow Usage ###

### Step 1: ### 
On your Benchmarking System, from the tf_cnn_benchmarks directory, copy repository and change into the tensorflow_benchmarks directory
```sh
git clone https://<Your Username>@bitbucket.org/exxsyseng/tensorflow_benchmarks.git
cd tensorflow_benchmarks
```

### Step 2: ###
Next copy the run-all-benchmarks.sh script back one directory to your tf_cnn_benchmarks directory
```sh
cp run-all-benchmarks.sh ../
```

### Step 3: ###
Next change directories to tf_cnn_benchmarks directory and run the run-all-benchmarks.sh script
```sh
cd ../
./run-all-benchmarks.sh
```

### Step 4: ###
After the benchmark runs complete, change directories into the tensorflow_benchmarks repo and run the copy_process_data.sh script
```sh
cd tensorflow_benchmarks
./copy_process_data
```

### Step 5: ###
After running the copy_Process_data.sh script. A copy of process_data.sh will be copied to all subdirectories with data results. To process data results, change directory into each data results directory and run process_data.ah
```sh
Exanple:

cd ../logs/synthetic/fp12/
./process_data.sh
```
### Process Data Script ###
The process_data.sh script will do the following
```sh
1. read each results file and extract required data: NAME,DATASET,MODEL,BATCH,GPU,GPU COUNT,IMAGES
2. data results will be parsed and populated into a CSV file
3. CSV file will be stored in /tmp
