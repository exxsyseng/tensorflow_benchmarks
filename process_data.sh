#!/bin/bash

# check that cmdline argument exist, if not error
if [[ $# -ne 1 ]]; then
  echo "Must specify Number of GPUs"
  echo "USAGE : $0 RTX-3080"
exit 2
fi

#### Variables ####
GPU=$1
RESULTS=2x_${GPU}_synthetic_fp16.csv
#RESULTS=2x_${GPU}_synthetic_fp32.csv
#RESULTS=2x_${GPU}_imagenet_fp16.csv
#RESULTS=2x_${GPU}_imagenet_fp32.csv

# create file with all result files
ls -1 tf* >> filelist

tfresults(){

# set variable for input file
FILE=/tmp/$1.$(date +%F_%T).log

# results file name will need to be manually changed
RESULTS=2x_$2_synthetic_fp16.csv
#RESULTS=2x_$2_synthetic_fp32.csv
#RESULTS=2x_$2_imagenet_fp16.csv
#RESULTS=2x_$2_imagenet_fp32.csv

# gather information from input file
echo $1 >> $FILE
cat $1 | grep Dataset | awk '{print $2}' >> $FILE
cat $1 | grep Model | awk '{print $2}' >> $FILE
cat $1 | grep device | awk '{print $1}' >> $FILE
echo $2 >> $FILE
echo $1 | awk -F'_' '{print $3}' >> $FILE
cat $1 | grep 'total images/sec' | awk '{print $3}' >> $FILE

# create output file
 cat $FILE | xargs | sed -e 's/[[:space:]]\{1,\}/,/g' >> $RESULTS 2>> /dev/null

}
export -f tfresults
parallel tfresults ::: $(cat filelist) ::: $GPU

# add header names to columns
#ex -sc '1i|NAME,DATASET,MODEL,BATCH,GPU,GPU COUNT,IMAGES' -cx 2x_${GPU}_synthetic_fp16.csv
ex -sc '1i|NAME,DATASET,MODEL,BATCH,GPU,GPU COUNT,IMAGES' -cx ${RESULTS}

# finally remove input files
rm -rf /tmp/tf_*
rm -rf filelist
